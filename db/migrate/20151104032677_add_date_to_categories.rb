class AddDateToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :date, :datetime
  end
end
